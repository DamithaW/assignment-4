#include <stdio.h>

 int main()
 {
     float int1, int2, int3;
     float sum, avg;

     printf("Enter the three Numbers: ");
     scanf("%f %f %f", &int1, &int2, &int3);
     sum = int1 + int2 + int3;
     avg = sum / 3;

     printf("Your numbers are: %.2f, %.2f and %.2f\n", int1, int2, int3);

     printf("Sum=%.2f\n", sum);
     printf("Average=%.2f\n",avg );

     return 0;
 }
