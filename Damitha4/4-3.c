#include <stdio.h>  
 int main()
{ 
 
 int x,y;

 printf("Enter the two numbers: ");
 scanf("%d %d", &x, &y);
     
 printf("Before swap x=%d y=%d",x,y);  
    
 x = x+y;    
 y = x-y;    
 x = x-y;
    
 printf("\nAfter swapping x=%d y=%d",x,y); 
   
 return 0;  
}
